# SALServiceSDK Framework
*A take home test by Ben Salanki.*  
  
**Summary**
* The project is a framework written for and in Swift.
* The built framework can be embedded in any iOS or macOS project.
* No third-party dependencies. The framework is set up by default to connect to [JSONPlaceholder](https://jsonplaceholder.typicode.com) but this can be changed in code.
* The project is fully documented and contains unit tests.

**General Information**
* SALService.swift contains the public methods for accessing the API.
* SALSerivce+Internal.swift contains the framework internal helper methods for loading creating and loading requests.
* SALServiceErrors.swift contains the errors the framework can throw.
* SALUser represents a user object that the framework can manipulate and have CRUD operations on.
* SALNetworkingProtocol is a protocol describing a function that the network handler of the SALService needs to implement.
* SALNetworkHandler is a barebones implementation of the SALNetworkingProtocol that uses URLSession to implement network communication.
* SALJSONCoding is a class with methods to encode and decode supported types.

Thank you for taking the time.  
Ben Salanki