//
//  SALServiceSDK.h
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for SALServiceSDK.
FOUNDATION_EXPORT double SALServiceSDKVersionNumber;

//! Project version string for SALServiceSDK.
FOUNDATION_EXPORT const unsigned char SALServiceSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SALServiceSDK/PublicHeader.h>


