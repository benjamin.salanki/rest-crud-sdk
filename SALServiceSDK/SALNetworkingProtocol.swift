//
//  SALNetworkingProtocol.swift
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import Foundation

/**
A protocol that a network handler of the SALService needs to conform to.
*/
public protocol SALNetworkingProtocol {
	/**
	Performs a network request on the API.
	
	Classes conforming to this protocol can use SALServiceError.underlyingError() with an associated Error object
	to pass errors not covered in the SALServiceError enum.
	
	- Parameters:
		- request: The URLRequest to use for the network request.
		- completionHandler: Called after the request completed processing.
	*/
	func perform(with request: URLRequest, completionHandler: @escaping (Result<Data, SALServiceError>) -> Void)
}
