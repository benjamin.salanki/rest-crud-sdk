//
//  SALNetworkHandler.swift
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30..
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import Foundation

/**
A basic network handler adopting SALNetworkingProtocol.

URLSession's dataTask is used for network communication.
If a request returns no data but is otherwise successful, the completionHandler returns with SALServiceError.noData.
*/
public class SALNetworkHandler: SALNetworkingProtocol {
	public init() {}
	
	public func perform(with request: URLRequest, completionHandler: @escaping (Result<Data, SALServiceError>) -> Void) {
		URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
			if let data = data {
				completionHandler(.success(data))
			} else if let error = error {
				completionHandler(.failure(error as? SALServiceError ?? .underlyingError(error)))
			} else {
				completionHandler(.failure(SALServiceError.noData))
			}
		}).resume()
	}
}
