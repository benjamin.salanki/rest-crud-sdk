//
//  SALService+Internal.swift
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import Foundation

extension SALService {
	
	/**
	CRUD methods supported by SALService.
	*/
	enum SALServiceMethod: String {
		/**
		GET method.
		*/
		case GET
		/**
		POST method.
		*/
		case POST
		/**
		PUT method.
		*/
		case PUT
		/**
		PATCH method.
		*/
		case PATCH
		/**
		DELETE method.
		*/
		case DELETE
	}
	
	/**
	Endpoints supported by SALService.
	
	The cases are appended to the API's baseURL as path components.
	*/
	enum SALServiceEndpoint: String {
		/**
		Endpoint for supporting CRUD operations on /users.
		*/
		case users
	}
	
	/**
	Convenience method for creating a URLRequest for an endpoint.
	
	- Parameters:
		- endpoint: The endpoint to append to the baseURL of the API.
		- item: The optional item identifier to append to the endpoint.
	*/
	func urlRequest(for endpoint: SALServiceEndpoint, item: Int? = nil) -> URLRequest {
		var url = baseURL.appendingPathComponent(endpoint.rawValue)
		if let item = item {
			url = url.appendingPathComponent(String(item))
		}
		return URLRequest(url: url)
	}

	/**
	Performs a load on a URLRequest.
	
	Attempts to parse the response into an object of type T.
	
	- Parameters:
		- urlRequest: The URLRequest to load.
		- completionHandler: Called after the request has completed processing.
	*/
	func load<T: Decodable>(_ urlRequest: URLRequest, completionHandler: @escaping (Result<T, SALServiceError>)-> Void) {
		var request = urlRequest
		request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-type")
		networkHandler.perform(with: request, completionHandler: { result in
			switch result {
			case .failure(let error):
				completionHandler(.failure(error))
			case .success(let data):
				let decodedData = SALJSONCoding.decode(data, resultType: T.self)
				switch decodedData {
				case .failure(let error):
					completionHandler(.failure(.underlyingError(error)))
				case .success(let data):
					completionHandler(.success(data))
				}
			}
		})
	}	
}
