//
//  SALUser.swift
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import Foundation

/**
Identifier of SALUser objects.
*/
public typealias SALUserID = Int

/**
Representation of a user object on the API.
*/
public struct SALUser: Codable, Equatable {
	public struct SALAddress: Codable, Equatable {
		public struct SALGeo: Codable, Equatable {
			public var lat: String?
			public var lng: String?
			
			public init(lat: String? = nil, lng: String? = nil) {
				self.lat = lat
				self.lng = lng
			}
		}
		
		public var street: String?
		public var suite: String?
		public var city: String?
		public var zipcode: String?
		public var geo: SALGeo?
		
		public init(street: String? = nil, suite: String? = nil, city: String? = nil, zipcode: String? = nil, geo: SALGeo? = nil) {
			self.street = street
			self.suite = suite
			self.city = city
			self.zipcode = zipcode
			self.geo = geo
		}
	}
	
	public struct SALCompany: Codable, Equatable {
		public var name: String?
		public var catchPhrase: String?
		public var bs: String?
		
		public init(name: String? = nil, catchPhrase: String? = nil, bs: String? = nil) {
			self.name = name
			self.catchPhrase = catchPhrase
			self.bs = bs
		}
	}
	
	public var id: SALUserID?
	public var name: String?
	public var username: String?
	public var email: String?
	public var address: SALAddress?
	public var phone: String?
	public var website: String?
	public var company: SALCompany?
	
	public init(id: SALUserID? = nil, name: String? = nil, username: String? = nil, email: String? = nil, address: SALAddress? = nil, phone: String? = nil, website: String? = nil, company: SALCompany? = nil) {
		self.id = id
		self.name = name
		self.username = username
		self.email = email
		self.address = address
		self.phone = phone
		self.website = website
		self.company = company
	}
	
	var isEmpty: Bool {
		return self.id == nil && self.name == nil && self.username == nil && self.email == nil && self.address == nil && self.phone == nil && self.website == nil && self.company == nil
	}
}
