//
//  ServiceError.swift
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import Foundation

/**
Errors specific to SALService.

SALServiceError.underlyingError() with an associated Error object can be used to pass errors not covered in the enum.
*/
public enum SALServiceError: Error {
	/**
	An unexpected error occured.
	*/
	case unexpectedError
	/**
	A malformed URL was encountered.
	*/
	case badURL
	/**
	A request returned with an empty response body.
	*/
	case noData
	/**
	A case with an associated Error object that can be used to pass underlying errors not handled in the SALServiceError
	enum.
	*/
	case underlyingError(Error)
}

extension SALServiceError: LocalizedError {
	public var errorDescription: String? {
		var errorDescription: String?
		switch self {
		case .unexpectedError:
			errorDescription = NSLocalizedString("sal.service.error.unexpectedError", value: "An unexpected error occured.", comment: "The error message for an unexpected error of type ServiceError.")
		case .badURL:
			errorDescription = NSLocalizedString("sal.service.error.badURL", value: "The URL was malformed.", comment: "The error message for a malformed URL error of type ServiceError.")
		case .noData:
			errorDescription = NSLocalizedString("sal.service.error.noData", value: "The response contained no data.", comment: "The error message for an empty response error of type ServiceError.")
		case .underlyingError(let error):
			errorDescription = error.localizedDescription
		}
		return errorDescription
	}
}
