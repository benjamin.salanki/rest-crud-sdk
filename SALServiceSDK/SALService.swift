//
//  SALService.swift
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import Foundation

public typealias SALUserListResult = (Result<[SALUser], SALServiceError>) -> Void
public typealias SALUserResult = (Result<SALUser?, SALServiceError>) -> Void

/**
Class for handling service requests.
*/

public class SALService {
	
	/**
	Network handler responsible for making the network calls.
	*/
	let networkHandler: SALNetworkingProtocol
	
	/**
	Base URL of the API.
	*/
	let baseURL: URL
	
	/**
	Intializer for the SALService.
	
	- Parameters:
		- networkHandler: The network handler responsible for making the network calls.
		- baseURL: Base URL of the API.
	
	- Returns: An initialized SALService, or nil if initialization failed.
	*/
	public init?(networkHandler: SALNetworkingProtocol = SALNetworkHandler(), baseURL: URL? = URL(string: "https://jsonplaceholder.typicode.com")) {
		guard let baseURL = baseURL else {
			return nil
		}
		self.networkHandler = networkHandler
		self.baseURL = baseURL
	}
	
	/**
	Fetches a list of SALUser objects.

	Makes a GET request on the users enpoint of the API.
	
	- Parameters:
		- completionHandler: Called when the request has finished.
	*/
	public func getUsers(_ completionHandler: @escaping SALUserListResult)  {
		load(urlRequest(for: .users), completionHandler: completionHandler)
	}
	
	/**
	Fetches a SALUser with the specified SALUserID.
	
	Makes a GET request on the users/{SALUserID} endpoint.
	If there is no user with the given identifier present, returns SALServiceError.noData.
	
	- Parameters:
		- id: The identifier used to uniquely identifier the SALUser.
		- completionHandler: Called when the request has finished.
	*/
	public func getUser(_ id: SALUserID, completionHandler: @escaping SALUserResult)  {
		load(urlRequest(for: .users, item: id), completionHandler: completionHandler)
	}
	
	/**
	Passes a SALUser to the API for creation.
	
	Makes a POST request on the users endpoint.
	The created SALUser is returned if the operation was successful, a SALServiceError otherwise.

	- Parameters:
		- user: The SALUser to pass to the API for creation.
		- completionHandler: Called when the request has finished.
	*/
	public func postUser(_ user: SALUser, completionHandler: @escaping SALUserResult) {
		var request = urlRequest(for: .users)
		request.httpMethod = SALServiceMethod.POST.rawValue
		let encodedUser = SALJSONCoding.encode(user)
		switch encodedUser {
		case .failure(let error):
			completionHandler(.failure(.underlyingError(error)))
		case .success(let data):
			request.httpBody = data
			load(request, completionHandler: completionHandler)
		}
	}
	
	/**
	Passes a SALUser to the API for idempotent updating.
	
	Makes a PUT request on the users/{user.id} endpoint.
	The updated SALUser is returned if the operation was successful, a SALServiceError otherwise.

	- Parameters:
		- user: The SALUser to pass to the API for updating.
		- completionHandler: Called when the request has finished.
	*/
	public func putUser(_ user: SALUser, completionHandler: @escaping SALUserResult) {
		var request = urlRequest(for: .users, item: user.id)
		request.httpMethod = SALServiceMethod.PUT.rawValue
		let encodedUser = SALJSONCoding.encode(user)
		switch encodedUser {
		case .failure(let error):
			completionHandler(.failure(.underlyingError(error)))
		case .success(let data):
			request.httpBody = data
			load(request, completionHandler: completionHandler)
		}
	}
	
	/**
	Passes a SALUser to the API for updating certain values.
	
	Makes a PATCH request on the users/{user.id} endpoint.
	Only non-nil values present in the user object will be updated on the API.
	The updated SALUser is returned if the operation was successful, a SALServiceError otherwise.
	
	- Parameters:
		- user: The SALUser to pass to the API for updating.
		- completionHandler: Called when the request has finished.
	*/
	public func patchUser(_ user: SALUser, completionHandler: @escaping SALUserResult) {
		var request = urlRequest(for: .users, item: user.id)
		request.httpMethod = SALServiceMethod.PATCH.rawValue
		let encodedUser = SALJSONCoding.encode(user)
		switch encodedUser {
		case .failure(let error):
			completionHandler(.failure(.underlyingError(error)))
		case .success(let data):
			request.httpBody = data
			load(request, completionHandler: completionHandler)
		}
	}

	/**
	Passes a SALUser to the API for deletion.
	
	Makes a DELETE request on the users/{user.id} endpoint.
	Nil is returned if the operation was successful, a SALServiceError otherwise.
	
	- Parameters:
	- user: The SALUser to pass to the API for deletion.
	- completionHandler: Called when the request has finished.
	*/
	public func deleteUser(_ user: SALUser, completionHandler: @escaping SALUserResult) {
		var request = urlRequest(for: .users, item: user.id)
		request.httpMethod = SALServiceMethod.DELETE.rawValue
		let encodedUser = SALJSONCoding.encode(user)
		switch encodedUser {
		case .failure(let error):
			completionHandler(.failure(.underlyingError(error)))
		case .success(let data):
			request.httpBody = data
			let completion: (Result<SALUser, SALServiceError>) -> Void = { result in
				switch result {
				case .failure(let error):
					completionHandler(.failure(error))
				case .success(_):
					completionHandler(.success(nil))
				}
			}
			load(request, completionHandler: completion)
		}
	}
}
