//
//  SALJSONCoding.swift
//  SALServiceSDK
//
//  Created by Benjamin Salanki on 2019. 04. 30..
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import Foundation

/**
A collection of methods for encoding and decoding supported types to and from JSON.
*/
class SALJSONCoding {
	/**
	Attempts to decode the passed JSON data into the specified type.
	
	- Parameters:
	- data: The data to decode from JSON.
	- resultType: The type of object to create from the passed data.
	
	- Returns: A Result type with either the decoded object or an error.
	*/
	class func decode<T: Decodable>(_ data: Data, resultType: T.Type) -> Result<T, Error> {
		do {
			let decoder = JSONDecoder()
			let response = try decoder.decode(resultType, from: data)
			return .success(response)
		} catch {
			return .failure(error)
		}
	}
	
	/**
	Attempts to encode the passed value into JSON data.
	
	- Parameters:
	- value: The value to encode into JSON.
	
	- Returns: A Result type with either the JSON encoded data or an error.
	*/
	class func encode<T: Codable>(_ value: T) -> Result<Data, Error> {
		do {
			let encoder = JSONEncoder()
			let data = try encoder.encode(value)
			return .success(data)
		} catch {
			return .failure(error)
		}
	}
}
