//
//  SALUserTests.swift
//  SALServiceSDKTests
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import XCTest
import SALServiceSDK

class SALUserTests: XCTestCase {
	
	static let street = "Street"
	static let suite = "Suite"
	static let city = "City"
	static let zipcode = "ZipCode"
	
	static let lat = "46.254931"
	static let lng = "20.150143"
	
	static let name = "name"
	static let username = "username"
	static let email = "email"
	static let phone = "phone"
	static let website = "website"
	
	static let companyName = "companyName"
	static let catchPhrase = "catchPhrase"
	static let bs = "bs"
	
	static let fullGeo = SALUser.SALAddress.SALGeo(lat: lat, lng: lng)
	static let emptyGeo = SALUser.SALAddress.SALGeo()
	static let fullAddress = SALUser.SALAddress(street: street, suite: suite, city: city, zipcode: zipcode, geo: fullGeo)
	static let emptyAddress = SALUser.SALAddress()
	static let fullCompany = SALUser.SALCompany(name: companyName, catchPhrase: catchPhrase, bs: bs)
	static let emptyCompany = SALUser.SALCompany()
	static let fullUser = SALUser(id: 1, name: name, username: username, email: email, address: fullAddress, phone: phone, website: website, company: fullCompany)
	static let emptyUser = SALUser(id: 2)

	func testGeo() {
		let fullGeo = SALUserTests.fullGeo
		XCTAssertNotNil(fullGeo)
		XCTAssertNotNil(fullGeo.lat)
		XCTAssertEqual(fullGeo.lat, SALUserTests.lat)
		XCTAssertNotNil(fullGeo.lng)
		XCTAssertEqual(fullGeo.lng, SALUserTests.lng)
		
		let emptyGeo = SALUserTests.emptyGeo
		XCTAssertNotNil(emptyGeo)
		XCTAssertNil(emptyGeo.lat)
		XCTAssertNil(emptyGeo.lng)
		
		for _ in 1..<Int.random(in: 5...20) {
			let hasLat = Bool.random()
			let hasLng = Bool.random()
			let geo = SALUser.SALAddress.SALGeo(lat: hasLat ? SALUserTests.lat : nil, lng: hasLng ? SALUserTests.lng : nil)
			XCTAssertNotNil(geo)
			if hasLat {
				XCTAssertNotNil(geo.lat)
				XCTAssertEqual(geo.lat, SALUserTests.lat)
			} else {
				XCTAssertNil(geo.lat)
			}
			if hasLng {
				XCTAssertNotNil(geo.lng)
				XCTAssertEqual(geo.lng, SALUserTests.lng)
			} else {
				XCTAssertNil(geo.lng)
			}
		}
	}
	
	func testAddress() {
		let fullAddress = SALUserTests.fullAddress
		XCTAssertNotNil(fullAddress)
		XCTAssertNotNil(fullAddress.street)
		XCTAssertEqual(fullAddress.street, SALUserTests.street)
		XCTAssertNotNil(fullAddress.suite)
		XCTAssertEqual(fullAddress.suite, SALUserTests.suite)
		XCTAssertNotNil(fullAddress.city)
		XCTAssertEqual(fullAddress.city, SALUserTests.city)
		XCTAssertNotNil(fullAddress.zipcode)
		XCTAssertEqual(fullAddress.zipcode, SALUserTests.zipcode)
		XCTAssertNotNil(fullAddress.geo)
		XCTAssertEqual(fullAddress.geo, SALUserTests.fullGeo)
		
		let emptyAddress = SALUserTests.emptyAddress
		XCTAssertNotNil(emptyAddress)
		XCTAssertNil(emptyAddress.street)
		XCTAssertNil(emptyAddress.suite)
		XCTAssertNil(emptyAddress.city)
		XCTAssertNil(emptyAddress.zipcode)
		XCTAssertNil(emptyAddress.geo)

		for _ in 1..<Int.random(in: 5...20) {
			let hasStreet = Bool.random()
			let hasSuite = Bool.random()
			let hasCity = Bool.random()
			let hasZipcode = Bool.random()
			let hasGeo = Bool.random()
			let address = SALUser.SALAddress(street: hasStreet ? SALUserTests.street : nil, suite: hasSuite ? SALUserTests.suite : nil, city: hasCity ? SALUserTests.city : nil, zipcode: hasZipcode ? SALUserTests.zipcode : nil, geo: hasGeo ? SALUserTests.fullGeo : nil)
			XCTAssertNotNil(address)
			if hasStreet {
				XCTAssertNotNil(address.street)
				XCTAssertEqual(address.street, SALUserTests.street)
			} else {
				XCTAssertNil(address.street)
			}
			if hasSuite {
				XCTAssertNotNil(address.suite)
				XCTAssertEqual(address.suite, SALUserTests.suite)
			} else {
				XCTAssertNil(address.suite)
			}
			if hasCity {
				XCTAssertNotNil(address.city)
				XCTAssertEqual(address.city, SALUserTests.city)
			} else {
				XCTAssertNil(address.city)
			}
			if hasZipcode {
				XCTAssertNotNil(address.zipcode)
				XCTAssertEqual(address.zipcode, SALUserTests.zipcode)
			} else {
				XCTAssertNil(address.zipcode)
			}
			if hasGeo {
				XCTAssertNotNil(address.geo)
				XCTAssertEqual(address.geo, SALUserTests.fullGeo)
			} else {
				XCTAssertNil(address.geo)
			}
		}
	}
	
	func testCompany() {
		let fullCompany = SALUserTests.fullCompany
		XCTAssertNotNil(fullCompany)
		XCTAssertNotNil(fullCompany.name)
		XCTAssertEqual(fullCompany.name, SALUserTests.companyName)
		XCTAssertNotNil(fullCompany.catchPhrase)
		XCTAssertEqual(fullCompany.catchPhrase, SALUserTests.catchPhrase)
		XCTAssertNotNil(fullCompany.bs)
		XCTAssertEqual(fullCompany.bs, SALUserTests.bs)
		
		let emptyCompany = SALUserTests.emptyCompany
		XCTAssertNotNil(emptyCompany)
		XCTAssertNil(emptyCompany.name)
		XCTAssertNil(emptyCompany.catchPhrase)
		XCTAssertNil(emptyCompany.bs)
		
		for _ in 1..<Int.random(in: 5...20) {
			let hasName = Bool.random()
			let hasCatchPhrase = Bool.random()
			let hasBS = Bool.random()
			let company = SALUser.SALCompany(name: hasName ? SALUserTests.companyName : nil, catchPhrase: hasCatchPhrase ? SALUserTests.catchPhrase : nil, bs: hasBS ? SALUserTests.bs : nil)
			XCTAssertNotNil(company)
			if hasName {
				XCTAssertNotNil(company.name)
				XCTAssertEqual(company.name, SALUserTests.companyName)
			} else {
				XCTAssertNil(company.name)
			}
			if hasCatchPhrase {
				XCTAssertNotNil(company.catchPhrase)
				XCTAssertEqual(company.catchPhrase, SALUserTests.catchPhrase)
			} else {
				XCTAssertNil(company.catchPhrase)
			}
			if hasBS {
				XCTAssertNotNil(company.bs)
				XCTAssertEqual(company.bs, SALUserTests.bs)
			} else {
				XCTAssertNil(company.bs)
			}
		}
	}
	
	func testUser() {
		let fullUser = SALUserTests.fullUser
		XCTAssertNotNil(fullUser)
		XCTAssertNotNil(fullUser.id)
		XCTAssertEqual(fullUser.id, 1)
		XCTAssertNotNil(fullUser.name)
		XCTAssertEqual(fullUser.name, SALUserTests.name)
		XCTAssertNotNil(fullUser.username)
		XCTAssertEqual(fullUser.username, SALUserTests.username)
		XCTAssertNotNil(fullUser.email)
		XCTAssertEqual(fullUser.email, SALUserTests.email)
		XCTAssertNotNil(fullUser.phone)
		XCTAssertEqual(fullUser.phone, SALUserTests.phone)
		XCTAssertNotNil(fullUser.website)
		XCTAssertEqual(fullUser.website, SALUserTests.website)
		XCTAssertNotNil(fullUser.address)
		XCTAssertEqual(fullUser.address, SALUserTests.fullAddress)
		XCTAssertNotNil(fullUser.company)
		XCTAssertEqual(fullUser.company, SALUserTests.fullCompany)

		let emptyUser = SALUserTests.emptyUser
		XCTAssertNotNil(emptyUser)
		XCTAssertNotNil(emptyUser.id)
		XCTAssertEqual(emptyUser.id, 2)
		XCTAssertNil(emptyUser.name)
		XCTAssertNil(emptyUser.username)
		XCTAssertNil(emptyUser.email)
		XCTAssertNil(emptyUser.phone)
		XCTAssertNil(emptyUser.website)
		XCTAssertNil(emptyUser.address)
		XCTAssertNil(emptyUser.company)

		for _ in 1..<Int.random(in: 5...20) {
			let hasName = Bool.random()
			let hasUsername = Bool.random()
			let hasEmail = Bool.random()
			let hasPhone = Bool.random()
			let hasWebsite = Bool.random()
			let hasAddress = Bool.random()
			let hasCompany = Bool.random()
			let user = SALUser(id: 3, name: hasName ? SALUserTests.name : nil, username: hasUsername ? SALUserTests.username : nil, email: hasEmail ? SALUserTests.email : nil, address: hasAddress ? SALUserTests.fullAddress : nil, phone: hasPhone ? SALUserTests.phone : nil, website: hasWebsite ? SALUserTests.website : nil, company: hasCompany ? SALUserTests.fullCompany : nil)
			XCTAssertNotNil(user)
			XCTAssertNotNil(user.id)
			XCTAssertEqual(user.id, 3)
			if hasName {
				XCTAssertNotNil(user.name)
				XCTAssertEqual(user.name, SALUserTests.name)
			} else {
				XCTAssertNil(user.name)
			}
			if hasUsername {
				XCTAssertNotNil(user.username)
				XCTAssertEqual(user.username, SALUserTests.username)
			} else {
				XCTAssertNil(user.username)
			}
			if hasEmail {
				XCTAssertNotNil(user.email)
				XCTAssertEqual(user.email, SALUserTests.email)
			} else {
				XCTAssertNil(user.email)
			}
			if hasPhone {
				XCTAssertNotNil(user.phone)
				XCTAssertEqual(user.phone, SALUserTests.phone)
			} else {
				XCTAssertNil(user.phone)
			}
			if hasWebsite {
				XCTAssertNotNil(user.website)
				XCTAssertEqual(user.website, SALUserTests.website)
			} else {
				XCTAssertNil(user.website)
			}
			if hasAddress {
				XCTAssertNotNil(user.address)
				XCTAssertEqual(user.address, SALUserTests.fullAddress)
			} else {
				XCTAssertNil(user.address)
			}
			if hasCompany {
				XCTAssertNotNil(user.company)
				XCTAssertEqual(user.company, SALUserTests.fullCompany)
			} else {
				XCTAssertNil(user.company)
			}
		}
	}
}
