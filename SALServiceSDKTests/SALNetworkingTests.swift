//
//  SALNetworkingTests.swift
//  SALServiceSDKTests
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import XCTest

class SALNetworkingTests: XCTestCase {

	func testNetworkHandlerSuccess() {
		let networkHandler = SALNetworkHandler()
		
		let expectation = self.expectation(description: "SALService.networkHandler.success")
		networkHandler.perform(with: URLRequest(url: URL(string: "https://google.com")!)) { result in
			XCTAssertNotNil(result)
			switch result {
			case .failure(let error):
				XCTAssert(false, "Received unexpected error during \(expectation.description) with message: \(error.localizedDescription)")
			case .success(let data):
				XCTAssertNotNil(data)
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 60, handler: nil)
	}

	func testNetworkHandlerFailure() {
		let networkHandler = SALNetworkHandler()
		
		let expectation = self.expectation(description: "SALService.networkHandler.failure")
		networkHandler.perform(with: URLRequest(url: URL(string: "http://some.strange.thing")!)) { result in
			XCTAssertNotNil(result)
			switch result {
			case .failure(let error):
				XCTAssertNotNil(error)
			case .success(let data):
				XCTAssert(false, "Received unexpected success result during \(expectation.description) with data: \(String(describing: String(data: data, encoding: .utf8)))")
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 60, handler: nil)
	}	
}
