//
//  SALServiceTests.swift
//  SALServiceSDKTests
//
//  Created by Benjamin Salanki on 2019. 04. 30.
//  Copyright © 2019. Benjamin Salanki. All rights reserved.
//

import XCTest

class MockedNetworkHandler: SALNetworkingProtocol {
	
	var expectedData: Data? = nil
	var expectedError: Error? = nil
	
	func perform(with request: URLRequest, completionHandler: @escaping (Result<Data, SALServiceError>) -> Void) {
		if let expectedData = expectedData {
			completionHandler(.success(expectedData))
		} else if let expectedError = expectedError {
			completionHandler(.failure(expectedError as? SALServiceError ?? SALServiceError.underlyingError(expectedError)))
		} else {
			completionHandler(.failure(.unexpectedError))
		}
	}
}

class SALServiceTests: XCTestCase {

	var networkHandler: MockedNetworkHandler!
	var service: SALService!
	
	override func setUp() {
		networkHandler = MockedNetworkHandler()
		service = SALService(networkHandler: networkHandler)
		networkHandler.expectedData = nil
		networkHandler.expectedError = nil
		XCTAssertNil(networkHandler.expectedData)
		XCTAssertNil(networkHandler.expectedError)
	}
	
	// MARK: - GET users
	func testGetUsersSuccess() {
		let expectedResult = users()
		switch SALJSONCoding.encode(expectedResult) {
		case .failure(let error):
			XCTAssert(false, "Failed to prepare data with message: \(error.localizedDescription)")
		case .success(let data):
			networkHandler.expectedData = data
		}
		XCTAssertNotNil(networkHandler.expectedData)
		XCTAssertNil(networkHandler.expectedError)
		
		let expectation = self.expectation(description: "service.getUsers.success")
		service.getUsers { result in
			switch result {
			case .failure(let error):
				XCTAssert(false, "\(expectation.description) failed with message: \(error.localizedDescription)")
			case .success(let data):
				XCTAssertNotNil(data)
				XCTAssertEqual(data, expectedResult)
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
	}

	func testGetUsersWithErrors() {
		errors().forEach { error in
			networkHandler.expectedError = error
			XCTAssertNil(networkHandler.expectedData)
			XCTAssertNotNil(networkHandler.expectedError)

			let expectation = self.expectation(description: "service.getUsers.failure")
			service.getUsers { result in
				switch result {
				case .failure(let returnedError):
					XCTAssertEqual(error.localizedDescription, returnedError.localizedDescription)
				case .success(_):
					XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
				}
				expectation.fulfill()
			}
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = "some string".data(using: .utf8)
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNotNil(networkHandler.expectedData)

		let expectation = self.expectation(description: "service.getUsers.failure")
		service.getUsers { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation.fulfill()
		}

		networkHandler.expectedError = nil
		networkHandler.expectedData = nil
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNil(networkHandler.expectedData)
		
		let expectation2 = self.expectation(description: "service.getUsers.failure")
		service.getUsers { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation2.fulfill()
		}

		waitForExpectations(timeout: 5, handler: nil)
	}
	
	// MARK: - GET user
	func testGetUserSuccess() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let expectedResult = user(with: userID)
		switch SALJSONCoding.encode(expectedResult) {
		case .failure(let error):
			XCTAssert(false, "Failed to prepare data with message: \(error.localizedDescription)")
		case .success(let data):
			networkHandler.expectedData = data
		}
		XCTAssertNotNil(networkHandler.expectedData)
		XCTAssertNil(networkHandler.expectedError)
		
		let expectation = self.expectation(description: "service.getUsers.success")
		service.getUser(userID) { result in
			switch result {
			case .failure(let error):
				XCTAssert(false, "\(expectation.description) failed with message: \(error.localizedDescription)")
			case .success(let data):
				XCTAssertNotNil(data)
				XCTAssertEqual(data, expectedResult)
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testGetUserWithErrors() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		errors().forEach { error in
			networkHandler.expectedError = error
			XCTAssertNil(networkHandler.expectedData)
			XCTAssertNotNil(networkHandler.expectedError)
			
			let expectation = self.expectation(description: "service.getUsers.failure")
			service.getUser(userID) { result in
				switch result {
				case .failure(let returnedError):
					XCTAssertEqual(error.localizedDescription, returnedError.localizedDescription)
				case .success(_):
					XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
				}
				expectation.fulfill()
			}
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = "some string".data(using: .utf8)
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNotNil(networkHandler.expectedData)
		
		let expectation = self.expectation(description: "service.getUser.failure")
		service.getUser(userID) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation.fulfill()
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = nil
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNil(networkHandler.expectedData)
		
		let expectation2 = self.expectation(description: "service.getUser.failure")
		service.getUser(userID) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation2.fulfill()
		}

		waitForExpectations(timeout: 5, handler: nil)
	}
	
	//MARK: - POST user
	
	func testPostUserSuccess() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let expectedResult = user(with: userID)
		switch SALJSONCoding.encode(expectedResult) {
		case .failure(let error):
			XCTAssert(false, "Failed to prepare data with message: \(error.localizedDescription)")
		case .success(let data):
			networkHandler.expectedData = data
		}
		XCTAssertNotNil(networkHandler.expectedData)
		XCTAssertNil(networkHandler.expectedError)
		
		let expectation = self.expectation(description: "service.postUser.success")
		service.postUser(expectedResult) { result in
			switch result {
			case .failure(let error):
				XCTAssert(false, "\(expectation.description) failed with message: \(error.localizedDescription)")
			case .success(let data):
				XCTAssertNotNil(data)
				XCTAssertEqual(data, expectedResult)
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testPostUserWithErrors() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let user = self.user(with: userID)
		errors().forEach { error in
			networkHandler.expectedError = error
			XCTAssertNil(networkHandler.expectedData)
			XCTAssertNotNil(networkHandler.expectedError)
			
			let expectation = self.expectation(description: "service.postUser.failure")
			service.postUser(user) { result in
				switch result {
				case .failure(let returnedError):
					XCTAssertEqual(error.localizedDescription, returnedError.localizedDescription)
				case .success(_):
					XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
				}
				expectation.fulfill()
			}
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = "some string".data(using: .utf8)
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNotNil(networkHandler.expectedData)
		
		let expectation = self.expectation(description: "service.postUser.failure")
		service.postUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation.fulfill()
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = nil
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNil(networkHandler.expectedData)
		
		let expectation2 = self.expectation(description: "service.postUser.failure")
		service.postUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation2.fulfill()
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}

	//MARK: - PUT user
	
	func testPutUserSuccess() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let expectedResult = user(with: userID)
		switch SALJSONCoding.encode(expectedResult) {
		case .failure(let error):
			XCTAssert(false, "Failed to prepare data with message: \(error.localizedDescription)")
		case .success(let data):
			networkHandler.expectedData = data
		}
		XCTAssertNotNil(networkHandler.expectedData)
		XCTAssertNil(networkHandler.expectedError)
		
		let expectation = self.expectation(description: "service.putUser.success")
		service.putUser(expectedResult) { result in
			switch result {
			case .failure(let error):
				XCTAssert(false, "\(expectation.description) failed with message: \(error.localizedDescription)")
			case .success(let data):
				XCTAssertNotNil(data)
				XCTAssertEqual(data, expectedResult)
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testPutUserWithErrors() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let user = self.user(with: userID)
		errors().forEach { error in
			networkHandler.expectedError = error
			XCTAssertNil(networkHandler.expectedData)
			XCTAssertNotNil(networkHandler.expectedError)
			
			let expectation = self.expectation(description: "service.putUser.failure")
			service.putUser(user) { result in
				switch result {
				case .failure(let returnedError):
					XCTAssertEqual(error.localizedDescription, returnedError.localizedDescription)
				case .success(_):
					XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
				}
				expectation.fulfill()
			}
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = "some string".data(using: .utf8)
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNotNil(networkHandler.expectedData)
		
		let expectation = self.expectation(description: "service.putUser.failure")
		service.putUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation.fulfill()
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = nil
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNil(networkHandler.expectedData)
		
		let expectation2 = self.expectation(description: "service.putUser.failure")
		service.putUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation2.fulfill()
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}

	//MARK: - PATCH user
	
	func testPatchUserSuccess() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let expectedResult = user(with: userID)
		switch SALJSONCoding.encode(expectedResult) {
		case .failure(let error):
			XCTAssert(false, "Failed to prepare data with message: \(error.localizedDescription)")
		case .success(let data):
			networkHandler.expectedData = data
		}
		XCTAssertNotNil(networkHandler.expectedData)
		XCTAssertNil(networkHandler.expectedError)
		
		let expectation = self.expectation(description: "service.patchUser.success")
		service.patchUser(expectedResult) { result in
			switch result {
			case .failure(let error):
				XCTAssert(false, "\(expectation.description) failed with message: \(error.localizedDescription)")
			case .success(let data):
				XCTAssertNotNil(data)
				XCTAssertEqual(data, expectedResult)
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testPatchUserWithErrors() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let user = self.user(with: userID)
		errors().forEach { error in
			networkHandler.expectedError = error
			XCTAssertNil(networkHandler.expectedData)
			XCTAssertNotNil(networkHandler.expectedError)
			
			let expectation = self.expectation(description: "service.patchUser.failure")
			service.patchUser(user) { result in
				switch result {
				case .failure(let returnedError):
					XCTAssertEqual(error.localizedDescription, returnedError.localizedDescription)
				case .success(_):
					XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
				}
				expectation.fulfill()
			}
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = "some string".data(using: .utf8)
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNotNil(networkHandler.expectedData)
		
		let expectation = self.expectation(description: "service.patchUser.failure")
		service.patchUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation.fulfill()
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = nil
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNil(networkHandler.expectedData)
		
		let expectation2 = self.expectation(description: "service.patchUser.failure")
		service.patchUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation2.fulfill()
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}

	//MARK: - DELETE user
	
	func testDeleteUserSuccess() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let user = self.user(with: userID)
		XCTAssertNil(networkHandler.expectedData)
		XCTAssertNil(networkHandler.expectedError)
		
		let expectation = self.expectation(description: "service.deleteUser.success")
		service.deleteUser(user) { result in
			switch result {
			case .failure(let error):
				XCTAssertEqual(error.localizedDescription, SALServiceError.unexpectedError.localizedDescription)
			case .success(let data):
				XCTAssert(false, "\(expectation.description) unexpectedly succeeded: \(String(describing: data))")
			}
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
	}
	
	func testDeleteUserWithErrors() {
		let userID: SALUserID = SALUserID.random(in: 1...10)
		let user = self.user(with: userID)
		errors().forEach { error in
			networkHandler.expectedError = error
			XCTAssertNil(networkHandler.expectedData)
			XCTAssertNotNil(networkHandler.expectedError)
			
			let expectation = self.expectation(description: "service.deleteUser.failure")
			service.deleteUser(user) { result in
				switch result {
				case .failure(let returnedError):
					XCTAssertEqual(error.localizedDescription, returnedError.localizedDescription)
				case .success(_):
					XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
				}
				expectation.fulfill()
			}
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = "some string".data(using: .utf8)
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNotNil(networkHandler.expectedData)
		
		let expectation = self.expectation(description: "service.deleteUser.failure")
		service.deleteUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation.fulfill()
		}
		
		networkHandler.expectedError = nil
		networkHandler.expectedData = nil
		XCTAssertNil(networkHandler.expectedError)
		XCTAssertNil(networkHandler.expectedData)
		
		let expectation2 = self.expectation(description: "service.deleteUser.failure")
		service.patchUser(user) { result in
			switch result {
			case .failure(let returnedError):
				XCTAssertNotNil(returnedError.localizedDescription)
			case .success(_):
				XCTAssert(false, "\(expectation.description) failed, expected error, got data.")
			}
			expectation2.fulfill()
		}
		
		waitForExpectations(timeout: 5, handler: nil)
	}

	//MARK: - JSON Encoding & Decoding
	
	func testJSONEncodingAndDecodingSuccess() {
		encodeAndDecode(self.users(), type: [SALUser].self)
		encodeAndDecode(self.user(with: SALUserID.random(in: 1...10)), type: SALUser.self)
	}
	
	func testJSONEncodingAndDecodingFailure() {
		let failedEncode = SALJSONCoding.encode("test string")
		XCTAssertNotNil(failedEncode)
		switch failedEncode {
		case .failure(let error):
			XCTAssertNotNil(error.localizedDescription)
		case .success(_):
			XCTAssert(false, "service.encode.failure failed, expected error, got data.")
		}
		
		let encodedString: Data = "some string".data(using: .utf8)!
		let failedDecode = SALJSONCoding.decode(encodedString, resultType: SALUser.self)
		XCTAssertNotNil(failedDecode)
		switch failedDecode {
		case .failure(let error):
			XCTAssertNotNil(error.localizedDescription)
		case .success(_):
			XCTAssert(false, "service.decode.failure failed, expected error, got data.")
		}
	}
}

//MARK: - Private
extension SALServiceTests {
	private static var underlyingError: Error = {
		var userInfo = Dictionary<String, Any>()
		userInfo[NSLocalizedDescriptionKey] = "Localized Description"
		return NSError(domain: "underlying.error", code: 23, userInfo: userInfo) as Error
	}()

	private func users() -> [SALUser] {
		var users = [SALUser]()
		for index in 0..<10 {
			users.append(user(with: index))
		}
		return users
	}
	
	private func user(with id: SALUserID) -> SALUser {
		return SALUser(id: id, name: "name", username: "username", email: "email", address: SALUser.SALAddress(street: "street", suite: "suite", city: "city", zipcode: "zipcode", geo: SALUser.SALAddress.SALGeo(lat: "46.254931", lng: "20.150143")), phone: "phone", website: "website", company: SALUser.SALCompany(name: "companyName", catchPhrase: "catchPhrase", bs: "bs"))
	}
	
	private func errors() -> [Error] {
		var errors = [Error]()
		errors.append(SALServiceError.badURL)
		errors.append(SALServiceError.noData)
		errors.append(SALServiceError.unexpectedError)
		errors.append(SALServiceError.underlyingError(SALServiceTests.underlyingError))
		errors.append(SALServiceTests.underlyingError)
		return errors
	}
	
	private func encodeAndDecode<T: Codable & Equatable>(_ object: T, type: T.Type) {
		let encodedObject = SALJSONCoding.encode(object)
		XCTAssertNotNil(encodedObject)
		switch encodedObject {
		case .failure(let error):
			XCTAssert(false, "service.encode.success failure with message \(error.localizedDescription)")
		case .success(let data):
			let decodedObject = SALJSONCoding.decode(data, resultType: type)
			XCTAssertNotNil(decodedObject)
			switch decodedObject {
			case .failure(let error):
				XCTAssert(false, "service.decode.success failure with message \(error.localizedDescription)")
			case .success(let data):
				XCTAssertNotNil(data)
				XCTAssertEqual(object, data)
			}
		}
	}
}
